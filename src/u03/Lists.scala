package u03

import u02.Modules.Person
import u02.Modules.Person.Student
import u02.Modules.Person.Teacher

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {

    case class Cons[E](head: E, tail: List[E]) extends List[E]

    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match {
      case Cons(h, t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()
    }

    @scala.annotation.tailrec
    def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
      case (Cons(_, t), n) if n>0 => drop(t, n-1)
      case _ => l
    }

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case Nil() => Nil()
    }

    def mapFM[A, B](l: List[A])(mapper: A => B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), flatMap(t)(a => Cons(mapper(a), Nil())))
      case Nil() => Nil()
    }

    @scala.annotation.tailrec
    def filterFM[A](l: List[A])(pred: A => Boolean): List[A] = l match {
      case Cons(h, t) if pred(h) => Cons(h, flatMap(t)(a => Cons(a, Nil())))
      case Cons(_, t) => filterFM(t)(pred)
      case Nil() => Nil()
    }

    def max(l: List[Int]): Option[Int] = l match {
      case Cons(h, Nil()) => Option.apply(h)
      case Cons(h, t) => Option.apply(math.max(h, max(t).get))
      case Nil() => Option.empty
    }

    def coursesTeachers(l: List[Person]): List[String] = flatMap(filter(l)(e => e.isInstanceOf[Teacher]))(e => List.Cons(e.asInstanceOf[Teacher].course, List.Nil()))

    @scala.annotation.tailrec
    def foldLeft[A, B](list: List[A])(start: B)(f: (B, A) => B): B = list match {
      case Cons(h, t) => foldLeft(t)(f(start, h))(f)
      case Nil() => start
    }

    private def reverse[A](list: List[A]): List[A] = foldLeft(list)(Nil(): List[A])((x, y) => Cons(y, x))

    def foldRight[A, B](list: List[A])(start: B)(f: (A, B) => B): B = {
      @scala.annotation.tailrec
      def fRight(list: List[A])(start: B)(f: (A, B) => B): B = list match {
        case Cons(h, t) => fRight(t)(f(h, start))(f)
        case Nil() => start
      }
      fRight(reverse(list))(start)(f)
    }
  }
}

object ListsMain extends App {
  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.mapFM(l)(_ + 1))
  val t = List.Cons[Person](Teacher("Viroli", "PPS"), List.Cons(Student("Nicolas", 56), List.Cons(Teacher("Ricci", "PCD"), List.Nil())))
  println(List.flatMap(l)(v => List.Cons(v+1, List.Nil())))
  println(List.flatMap(l)(v => List.Cons(v+1, List.Cons(v+2, List.Nil()))))
  println(List.max(l)) //30
  println(List.max(List.Nil())) //None

  println(List.coursesTeachers(t))

  println(List.sum(l)) // 60
  import List._
  import u03.Lists.List
  println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >=20)) // 20,30
  println(filterFM[Int](l)(_ >=20)) // 20,30

  //Test drop
  val dropList = List.Cons(1, List.Cons(2, List.Cons(3, List.Nil())))
  println(List.drop(dropList, 0))

  val lst = List.Cons(3, List.Cons(7, List.Cons(1, List.Cons(5, Nil()))))
  println(List.foldLeft(lst)(0)(_-_)) // -16
  println(List.foldRight(lst)(0)(_-_)) // -8

}